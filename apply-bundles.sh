#!/bin/bash
#
# Apply a set of Git bundles created by mkbundles.sh
#

: ${PROJDIR=$HOME/src}

dir="$1"
[[ -n "$dir" && -d "$dir" ]] || {
    echo "Usage: $(basename $0) bundledir" 1>&2
    exit 1
}

cd $dir
for f in *.bundle; do
    # Each bundle file is named $REPO.bundle
    repo="${PROJDIR}/${f%.*}"
    loc=$(pwd)
    if [[ -d "$repo" ]]; then
        # Get the active branch
        (
            cd $repo
            b=$(git rev-parse --abbrev-ref HEAD)
            git pull $loc/$f $b
        )
    else
        echo "$repo not found, cloning"
        git clone $loc/$f $repo
    fi
done
