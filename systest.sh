#!/bin/bash
#
# Create a couple of Tmux windows to sample sensors
# and monitor the Sensor Manager output.
#

SESSION_NAME="systest"

if ! tmux has-session -t $SESSION_NAME; then
    tmux new-session -s $SESSION_NAME -n main -d
    tmux send-keys -t $SESSION_NAME 'sensmon' C-m
    tmux split-window -v -t ${SESSION_NAME}:0
    tmux new-window -n logs -t $SESSION_NAME
    tmux send-keys -t ${SESSION_NAME}:1 'tail -f ~/logs/sensormgr/current' C-m
    tmux select-window -t ${SESSION_NAME}:0
fi
tmux attach -t $SESSION_NAME
