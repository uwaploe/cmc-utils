#!/bin/sh
#
# Various utility functions.
#
TSCTL_TIMEOUT=5

# Run the power script with a timeout
xpower ()
{
    sudo -E timeout -s 9 $TSCTL_TIMEOUT $HOME/bin/power "$@"
    if [[ $? = "124" ]]; then
        echo "tsctl server not responding, restarting"
        sudo killall -9 tsctl
        sudo /usr/local/bin/tsctl --server
        sudo -E timeout -s 9 $TSCTL_TIMEOUT $HOME/bin/power "$@"
    fi
}

my_lockfile ()
{
    local tempfile lockfile

    tempfile="$1.$$"
    lockfile="$1.lock"
    echo $$ 1> $tempfile 2> /dev/null || {
        echo "Cannot access directory `dirname $tempfile`"
        return 1
    }
    ln $tempfile $lockfile 1> /dev/null 2>&1 && {
        rm -f $tempfile
        return 0
    }
    kill -0 `cat $lockfile` 1> /dev/null 2>&1 && {
        rm -f $tempfile
        return 1
    }
    echo "Removing stale lock file $lockfile"
    rm -f $lockfile
    ln $tempfile $lockfile 1> /dev/null 2>&1 && {
        rm -f $tempfile
        return 0
    }
    rm -f $tempfile
    return 1
}

USB_PIDDIR="$HOME/run/usb"
mkdir -p $USB_PIDDIR

#
# Power on the USB-serial converter and add the specified process the
# list of processes using the device
#
start_usb ()
{
    local mypid

    mypid="$1"
    until my_lockfile $USB_PIDDIR; do
	    sleep 1
    done

    xpower -on usb
    sleep 2
    xpower -on hub
    touch $USB_PIDDIR/$mypid

    rm -f ${USB_PIDDIR}.lock
}

#
# Remove the specified process from the list of USB/serial users and power off
# the device if no users are left
#
stop_usb ()
{
    local mypid p count

    mypid="$1"
    until my_lockfile $USB_PIDDIR; do
	    sleep 1
    done

    if [[ -d $USB_PIDDIR ]]; then
	    rm -f $USB_PIDDIR/$mypid
	    count=0
	    for p in $USB_PIDDIR/*; do
	        if kill -0 ${p##*/} 1> /dev/null 2>&1; then
                # Count running processes
                ((count++))
	        else
		        rm -f $p
	        fi
	    done
	    if ((count == 0)); then
            echo "Powering off USB"
	        xpower -off hub
            sleep 2
            xpower -off usb
	    fi
    fi

    rm -f ${USB_PIDDIR}.lock
}

file_wait ()
{
    local n file verbose limit

    verbose=
    if [[ "$1" = "-v" ]]; then
        verbose=1
        shift
    fi

    file="$1"
    n=$2
    [[ $file && $n ]] || return 1
    limit=$n
    while sleep 1; do
        [[ -e "$file" ]] && break
        if ((--n <= 0)); then
            return 1
        fi
        [[ "$verbose" ]] && \
            echo "$(( (limit - n)*100/limit ))"
    done
    [[ "$verbose" ]] && echo "100"
    return 0
}

set_gps_time ()
{
    local pid timeout dt retval

    timeout="$1"
    retval=1
    # Stop NTP
    [[ -x /etc/init.d/ntp ]] && sudo /etc/init.d/ntp stop

    # Wait for an events/clockcheck message. The payload of this
    # message is the difference between the system clock time
    # and the GPS time.
    dt=
    while read -t $timeout dt; do
        [[ -n "$dt" ]] && break
    done < <(mosquitto_sub -t events/clockcheck 2> /dev/null)

    if [[ -n "$dt" ]]; then
        retval=0
	    echo "Clock adjustment: $dt seconds"
	    date -u --set="$dt seconds"
	    echo "done."
    fi
    [[ -x /etc/init.d/ntp ]] && sudo /etc/init.d/ntp start
    return $retval
}

wait_for_tsctl ()
{
    local n limit

    n=$1
    [[ $n ]] || return 1
    limit=$n
    if ! tsctl @localhost system echonumber 1 1> /dev/null 2>&1; then
        echo "Waiting for TSCTL server"
        while sleep 1; do
            tsctl @localhost system echonumber 1 1> /dev/null 2>&1 && break
            if ((--n <= 0)); then
                return 1
            fi
        done
    fi
    return 0
}

wait_for_msg ()
{
    topic="$1"
    [[ -z "$topic" ]] && return 1
    count=${2:-1}
    while read msg; do
        echo "$msg"
        ((count--))
        ((count == 0)) && break
    done < <(stdbuf -oL mosquitto_sub -h localhost -t "$topic")
    return 0
}
