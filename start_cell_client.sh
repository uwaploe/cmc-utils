#!/bin/bash
#
# Power-on the cell modem, connect to the shore-side server,
# upload data, then power the modem off.
#
PATH=$HOME/bin:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin
export PATH

CELL="$HOME/run/cell"

[[ -e $HOME/bin/functions.sh ]] && . $HOME/bin/functions.sh
# Load the defaults
DEFAULTS=$HOME/config/chaba-defaults
[[ -r $DEFAULTS ]] && . $DEFAULTS

# This allows for bench testing where we are already
# connected to the Internet.
if fping -c 2 -q $DATAHOST 2> /dev/null; then
    echo "Bench test mode"
    cell_client.ksc
    exit $?
fi

my_lockfile $CELL || {
    echo "Cell gateway in use"
    exit 1
}

xpower -on cell
trap "xpower -off cell;rm -f ${CELL}.lock;exit 1" 1 2 3 15
echo "Cell gateway powered on"
sleep $CELL_BOOT_TIME

echo "Enabling Ethernet interface"
sudo enet.sh up

echo "Waiting for Internet connection"
t0=$(date +%s)
until fping -c 2 -q $DATAHOST 2> /dev/null;do
    t1=$(date +%s)
    if (((t1 - t0) > CELL_CONNECT_TIMEOUT)); then
        echo "No internet connection available"
        sudo enet.sh down
        xpower -off cell
        # Release the lock
        rm -f ${CELL}.lock
        exit 1
    fi
    sleep 1
done
t1=$(date +%s)
echo "Cell connection setup time $((t1 - t0)) seconds"

# Run the client
cell_client.ksc
status=$?

[[ -n "$NTP_SERVER" ]] && {
    sudo /etc/init.d/ntp stop
    sudo /usr/sbin/ntpdate -b -t ${NTP_TIMEOUT:-20} $NTP_SERVER
    sudo /etc/init.d/ntp start
}

echo "Disabling Ethernet interface"
sudo enet.sh down

xpower -off cell
# Release the lock
rm -f ${CELL}.lock

exit $status
