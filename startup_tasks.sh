#!/bin/bash

VIRTUALENV="$HOME/.venvs/mc"
PATH=$VIRTUALENV/bin:$HOME/bin:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin
export PATH

[[ -e $HOME/bin/functions.sh ]] && . $HOME/bin/functions.sh

# Load the defaults
DEFAULTS=$HOME/config/chaba-defaults
[[ -r $DEFAULTS ]] && . $DEFAULTS

echo "Start-up tasks"
# Wait for the tsctl server to start and then power-on the
# devices listed in the $ALWAYS_ON array.
if wait_for_tsctl 30; then
    power -on "${ALWAYS_ON[@]}"
fi
