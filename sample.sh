#!/bin/bash
#
# Sample one or more ChaBa sensors. Sensor names may be passed on the
# command-line, otherwise, the sampling schedule is checked to see which
# sensors need to be sampled in the current minute of the hour.
#

USB_DEV="/dev/ttyUSB7"
SENSOR_CFG="$HOME/config/sensors.bin"
START_DIR="$HOME/bin/start.d"
PRESAMPLE_DIR="$HOME/bin/presample.d"
POSTSAMPLE_DIR="$HOME/bin/postsample.d"
END_DIR="$HOME/bin/end.d"
CFGDIR="$HOME/config"
SAMPLE_TIMEOUT=180
VIRTUALENV="$HOME/.venvs/mc"

PATH=$VIRTUALENV/bin:$HOME/bin:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin
export PATH

[[ -e $HOME/bin/functions.sh ]] && . $HOME/bin/functions.sh

# Load the defaults
DEFAULTS=$HOME/config/chaba-defaults
[[ -r $DEFAULTS ]] && . $DEFAULTS

pubmsg ()
{
    if [[ -n "$1" ]]; then
        if [[ -n "$2" ]]; then
            mosquitto_pub -h localhost -q 1 -t events/sampling/$1 -m "$2"
        else
            mosquitto_pub -h localhost -q 1 -t events/sampling/$1 -m "$1"
        fi
    fi
}

# Only one instance of this script can be running.
my_lockfile $SENSOR_CFG || {
    echo "Sampling already in progress"
    exit 0
}
LOCKFILE="${SENSOR_CFG}.lock"

# The file ~/.nosample is automatically created when someone logs-in to
# the sysop account from the console. It contains the process-id of the
# shell. If the user has logged out, the process no longer exists and we
# remove the file.
if [[ -e $HOME/.nosample ]]; then
    pid=$(<$HOME/.nosample)
    if kill -0 $pid 1>/dev/null 2>&1; then
        echo "Sampling disabled"
        rm -f $LOCKFILE
        pubmsg done
        exit 0
    else
        rm -f $HOME/.nosample
    fi
fi

# Can also disable via configuration variable in $DEFAULTS
if [[ -n "$DISABLE_SAMPLING" ]]; then
    echo "Sampling disabled"
    rm -f $LOCKFILE
    pubmsg done
    exit 0
fi

# Because this script is run by cron, it's possible that we might be
# started before the TSCTL server is running. We need to wait for it as it
# provides the interface for manipulating the DIO lines.
wait_for_tsctl 10 || {
    echo "TSCTL server not running, sampling disabled"
    # Try to start it manually so it will be running on the next interval.
    sudo /usr/local/bin/tsctl --server
    rm -f $LOCKFILE
    pubmsg done
    exit 1
}

# Sensor names from the command-line
SENSORS=("$@")
# If no sensor names were specified on the command line, lookup
# the sensors scheduled for this minute.
if [[ -z "$SENSORS" ]]; then
    m=$(date +%M)
    SENSORS=($(lookup_sensors.awk minute=$m $CFGDIR/sample_times.txt))
    echo "Sensor list: ${SENSORS[@]}"
fi

args=()
need_usb=
use_wqm=
for s in "${SENSORS[@]}"; do
    args+=("--arg=$s")
    # WQM has a long warm-up, power it on early
    [[ "$s" = "wqm/1" ]] && {
        echo "Power on wqm/1"
        xpower -on wqm/1
        use_wqm=1
    }
    # If the sensor is attached to the USB/serial adapter (hub), we
    # need to power-on the bus and the adapter.
    [[ "${NEED_USB[$s]}" = "yes" ]] && need_usb=1
done

[[ -d $START_DIR ]] && run-parts "${args[@]}" $START_DIR

THIS_PID=$$
[[ -n "$need_usb" ]] && {
    echo "Power on USB/serial adapter"
    start_usb $THIS_PID
    file_wait $USB_DEV 60 || {
        echo "USB/serial hub not powering on. Aborting"
        echo "Power off wqm/1"
        [[ -n $use_wqm ]] && xpower -off wqm/1
        stop_usb $THIS_PID
        rm -f $LOCKFILE
        pubmsg done
        exit 1
    }
    echo "USB/serial adapter on"
}

trap "[[ -n $use_wqm ]] && xpower -off wqm/1;stop_usb $THIS_PID;rm -f $LOCKFILE; exit 1" 1 2 3 15

[[ -d $PRESAMPLE_DIR ]] && run-parts "${args[@]}" $PRESAMPLE_DIR

# Start a background job to wait for the sensors to finish
sensor_events --timeout $SAMPLE_TIMEOUT disable "${SENSORS[@]}" &
child="$!"
# Allow time for sensor_events to start listening before we sample.
sleep 3
for s in "${SENSORS[@]}"; do
    opts="-n"
    [[ -n "${SAMPLE_COUNT[$s]}" ]] && opts="-m count=${SAMPLE_COUNT[$s]}"
    mosquitto_pub -q 2 -t sample/${s} $opts
done
echo "Waiting for sampling to complete"
wait $child
echo "Sampling done (exit status = $?)"
[[ -n $use_wqm ]] && {
    echo "Power off wqm/1"
    xpower -off wqm/1
}

stop_usb $THIS_PID

[[ -d $POSTSAMPLE_DIR ]] && run-parts "${args[@]}" $POSTSAMPLE_DIR

rm -f $LOCKFILE

[[ -d $END_DIR ]] && run-parts $END_DIR

pubmsg done
exit 0
