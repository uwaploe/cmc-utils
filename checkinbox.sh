#!/bin/bash
#
# Check inbox for files downloaded from the Shore.
#
INBOX=$HOME/INBOX
TMPDIR=$HOME/unpack

sysupdate ()
{
    #
    # A system update file is a compressed tar archive containing two or
    # three compressed tar archives; files.tar.gz which contains the new
    # files to install, bundles.tar.gz which contains Git bundles, and
    # control.tar.gz which may contain the shell scripts 'preinst' and
    # 'postinst'. If present, these scripts are executed before and after
    # the file installation.
    #
    file=$1
    here=$(pwd)
    mkdir -p $TMPDIR
    cd $TMPDIR
    tar -xzf $file
    [[ -e control.tar.gz ]] && tar -xzf control.tar.gz
    [[ -x ./preinst ]] && ./preinst
    if [[ -f files.tar.gz ]]; then
        (cd $HOME && tar xvzf $TMPDIR/files.tar.gz)
        rm -f files.tar.gz
    fi
    if [[ -f bundles.tar.gz ]]; then
        tar xzf bundles.tar.gz
        $HOME/bin/apply-bundles.sh bundles
        rm -rf bundles
        rm -f bundles.tar.gz
    fi
    [[ -x ./postinst ]] && ./postinst
    rm -rf control.tar.gz postinst preinst
    rm -f $file
    cd $here
}

echo "Checking inbox"

for f in $INBOX/*
do
    case "$f" in
	    *.upd)
	        echo "System update"
	        sysupdate $f
	        ;;
    esac
done
