#!/bin/bash
#
# Watchdog for the tsctl server process
#
export PATH=/usr/local/bin:/usr/bin:/bin:$PATH

restart_server () {
    echo "Restarting tsctl"
    sudo killall -9 tsctl
    sudo tsctl --server
}

TIMEOUT=10
# Allow $TIMEOUT seconds for a response.
sudo timeout -s 9 $TIMEOUT \
     /usr/local/bin/tsctl @localhost dio getasync pa9 1> /dev/null 2>&1
if [[ $? = "124" ]]; then
    restart_server
fi
