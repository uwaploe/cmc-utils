#!/usr/bin/env bash
#
# Create a system update package for the ChaBa Mooring Controller.
#

usage () {
    echo "Usage: cmc-update.sh dir [pkgname]" 1>&2
    exit 1
}

: ${PROJDIR=$HOME/projects/Moorings}
: ${GITREV_URL=http://waves.apl.uw.edu/~mooring/chaba/raw/00latest/gitrevs}

if which gnutar 1> /dev/null 2>&1; then
    TAR=gnutar
else
    TAR=tar
fi

[[ "$1" = "--fetchrevs" ]] && {
    fetchrevs="yes"
    shift
}

dir="$1"
[[ -z "$dir" ]] && usage
pkgname="$2"
[[ -z "$pkgname" ]] && pkgname="${dir}.upd"
pkgname="$PWD/$pkgname"

# Set-up the build directory
builddir="$TMPDIR/updbuild.$$"
repodir="$TMPDIR/repos"
mkdir -p $builddir $repodir
trap "rm -rf $builddir $repodir;exit 0" 0 1 2 3 15

[[ -n "$fetchrevs" ]] && [[ -d "$dir/CONTROL" ]] && \
    curl -s -X GET $GITREV_URL > "$dir/CONTROL/gitrevs"

# Create Git bundles for any out-of-date repositories
if [[ -f "$dir/CONTROL/gitrevs" ]]; then
    target="$builddir/bundles"
    mkdir -p "$target"
    n=0
    while read repo rev url; do
        ref=
        [[ -d $PROJDIR/$repo ]] && ref="--reference $PROJDIR/$repo"
        git clone $ref $url $repodir/$repo
        # Find the most recent branch
        b=$(GIT_DIR="$repodir/$repo/.git" git for-each-ref \
                   --sort=-committerdate --format='%(refname:short)'|\
                head -n 1 | sed -e 's!origin/!!')
        [[ -n "$b" ]] && GIT_DIR="$repodir/$repo/.git" git checkout "$b"
        #b=$(GIT_DIR="$repodir/$repo/.git" git rev-parse --abbrev-ref HEAD)
        if [[ "$rev" = "0" ]]; then
            GIT_DIR="$repodir/$repo/.git" git bundle create \
                   "$target/${repo}.bundle" ${b} 2> /dev/null
        else
            GIT_DIR="$repodir/$repo/.git" git bundle create \
                   "$target/${repo}.bundle" ${rev}..${b} 2> /dev/null
        fi
        [[ -s "$target/${repo}.bundle" ]] && {
            git bundle list-heads "$target/${repo}.bundle"
            ((n++))
        }
    done < "$dir/CONTROL/gitrevs"
    if ((n > 0)); then
        echo "Creating Git bundles"
        $TAR -C $builddir -c -v -z -f $builddir/bundles.tar.gz bundles
    else
        echo "All Git repos are up-to-date"
    fi
    rm -rf "$target"
fi

if [[ -d "$dir/CONTROL" ]]; then
    chmod a+x "$dir/CONTROL/preinst" "$dir/CONTROL/postinst"
    (cd "$dir/CONTROL" && \
         $TAR --exclude=gitrevs -czvf $builddir/control.tar.gz .)
fi

[[ -d "$dir/FILES" ]] && \
    (cd "$dir/FILES" && $TAR -cvzf $builddir/files.tar.gz .)
(cd "$builddir" && $TAR -czf "$pkgname" ./*.tar.gz)

echo "Packaged contents of \"$dir\" into \"$pkgname\""
