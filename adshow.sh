#!/bin/bash
#
# Read and display A/D values on a TS4200 or TS4800
#
PATH=$PATH:/usr/local/bin
export PATH


form_init () {
    title="$1"
    shift
    n=${#title}
    col=$(($(tput cols)/2 - $n/2))
    tput cup 0 $col
    echo -n $title
    row=4
    col=20
    tput bold
    for label
    do
        tput cup $row $col
        echo -n $label
        row=$((row + 1))
    done
    tput cup 0 0
    echo -n "^c to exit"
    tput sgr0
}

form_update () {
    row=4
    col=54
    for value
    do
        tput cup $row $col
        tput el
        printf "%s" $value
        row=$((row + 1))
    done
}

display_init () {
    tput civis
    tput clear
    tput sc
}

display_restore () {
    tput cnorm
    tput clear
    tput rc
}

arg=
[[ "$(arch)" = "armv7l" ]] && arg="-ts4800=true"

display_init
pipe="/tmp/adc"
rm -f $pipe
mkfifo $pipe
adread $arg "$@" > $pipe &
child="$!"
trap "kill $child 2> /dev/null;rm -f $pipe;display_restore;exit 0" 0 1 2 3 15

first="yes"
while read line; do
    if [[ $first ]]; then
        form_init "$BOARD A/D Input" $(cut -f3- -d, <<<"$line"|tr "," " ")
        first=
    else
        form_update $(cut -f3- -d, <<<"$line"|tr "," " ")
    fi
done < $pipe
