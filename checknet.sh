#!/bin/bash

HOST="128.95.97.120"
echo -n 'Waiting for network '
t0=$(date +%s)
until fping -c 2 -q $HOST 2> /dev/null;do
    sleep 1
    echo -n '.'
done
t1=$(date +%s)
echo " done ($((t1 - t0)) seconds)"
