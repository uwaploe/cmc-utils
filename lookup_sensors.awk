#!/usr/bin/awk -f
#
# Read a sample schedule file which maps a sensor name to
# the minute of the hour that it should be sampled and print
# the list of sensors for a specific minute. The minute is passed
# on the command line as "minute=N" where N is a number between
# 0 and 59.
#
BEGIN {
    FS=","
}

$0 !~ /^#/ {
    for(i = 2;i <= NF;i++)
        sensors[$i] = sensors[$i] " " $1;
}

END {
    print sensors[minute+0];
}
