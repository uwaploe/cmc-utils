#!/usr/bin/kermit +
#
# Transfer files over a cell-phone connection
#
local \%i outbox inbox routbox rinbox logfile login_attempts

define giveup { bye, hangup, close, exit \%1 {FATAL: \%2 (Connection closed)} }

#
# INBOX and OUTBOX directories for this system.
#
define dir_setup {
    .rinbox := ~/chaba/INBOX
    .routbox := ~/chaba/OUTBOX
    remote mkdir \m(rinbox)
    remote mkdir \m(routbox)
}

# Source of files to be uploaded
.outbox := \v(home)OUTBOX
# Destination of files to be downloaded
.inbox := \v(home)INBOX
# Number of login attempts
.login_attempts := 2
.logfile := \v(home)logs/cell_xfer.log

take \v(home)config/creds.ksc
xif fail {
    .user = mooring
    .host = "128.95.97.213"
    .port = 1649
}

# Process macro=value command-line arguments
for \%i 1 \v(argc)-1 1 {
    if not \fkeywordval(\&_[\%i]) end 1 Bad parameter: "\&_[\%i]"
}

while not def password {
    askq password {Password: }
}

# Import local macro library
take \v(home)bin/lib.ksc

set exit warning off
set input echo off

# Log all file transfers
set transfer display none
set transaction-log brief
log transactions \m(logfile) new

# Overwrite incoming files with same name
set file collision overwrite

# Connect to host
set host \m(host) \m(port)

for \%i 1 \m(login_attempts) 1 {
    remote login \m(user) \m(password)
    xif fail {
        pause 5
        echo {Retrying remote login}
        continue
    } else {
        break
    }
}

if > \%i \m(login_attempts) exit 1 {Remote login failed!}

# Create INBOX/OUTBOX directories
dir_setup

remote cd \m(routbox)
xif fail {
    echo {Cannot access remote outbox}
} else {
    cd \m(inbox)
    query kermit rfiles(*)
    if > \v(query) 0 {
        retrieve *
    }
}

.rc := 0
remote cd \m(rinbox)
xif fail {
    .rc := 1
    echo {Cannot access remote inbox}
} else {
    cd \m(outbox)
    send_all
    .rc := \v(status)
}
# Logout
bye

exit \m(rc)
