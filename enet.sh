#!/bin/bash
#
# Bring the Ethernet interface up or down
#

: ${MODULE=smsc95xx}
: ${IFACE=eth0}

[[ -e /initrd/ts4200.subr ]] || {
    echo "TS-4200 script library not found" 1>&2
    exit 1
}

[[ "$(id -u)" = "0" ]] || {
    echo "You must be 'root' to run this script" 1>&2
    exit 1
}

. /initrd/ts4200.subr

case "$1" in
    up|on)
        eth_on
        modprobe $MODULE
        ifup $IFACE
        ;;
    down|off)
        ifdown $IFACE
        modprobe -r $MODULE
        eth_off
        ;;
    *)
        echo "Usage: $(basename $0) up|down" 1>&2
        exit 1
        ;;
esac
