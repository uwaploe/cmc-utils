#
# Makefile to install utility scripts.
#
SHELL = /bin/bash
INSTALL = install
BINDIR = $(HOME)/bin
SCRIPTS = power.sh adshow.sh getrevs.sh \
    apply-bundles.sh functions.sh lookup_sensors.awk \
    start_cell_client.sh \
	lib.ksc cell_client.ksc iridium_call.ksc \
    systest.sh checkinbox.sh sample.sh tswatch.sh \
    startup_tasks.sh
DIRS = dataxfer.d
SYSSCRIPTS = enet.sh
SYSBINDIR = /usr/local/bin

.PHONY: install install-dirs install-sys

all: install install-dirs install-sys

install: $(SCRIPTS)
	$(INSTALL) -d $(BINDIR)
	$(INSTALL) -m 755 $^ $(BINDIR)/
	ln -sf $(BINDIR)/power.sh $(BINDIR)/power
	ln -sf $(BINDIR)/adshow.sh $(BINDIR)/adshow

install-dirs: $(DIRS)
	$(INSTALL) -d $(BINDIR)
	cp -av $^ $(BINDIR)/

install-sys: $(SYSSCRIPTS)
	sudo $(INSTALL) -d $(SYSBINDIR)
	sudo $(INSTALL) -m 755 $^ $(SYSBINDIR)/
